#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>


void SalvaMatriz(char *NomeArquivo, char *texto, int quantidade, double tempo) {
    FILE *fp = fopen(NomeArquivo, "a");
    if (fp == NULL) {
        printf("\n::: Erro abrindo arquivo '%s''!\n", NomeArquivo);
        exit(EXIT_FAILURE);
    }

    fprintf(fp, " Processamento teste %s matriz com"
                " quantidade de %d com tempo de %f segundos \n", texto, quantidade, tempo);
    fclose(fp);

}

void MostraMatriz(int Altura, int Largura, int *M, char *Titulo, int abordagem) {
    int L, C; //Linha e Coluna

    printf(" %s \n", Titulo);
    for (L = 0; L < Altura; L++) {
        for (C = 0; C < Largura; C++) {
            if (abordagem == 1) {

                printf(" %2d ", M[L * Largura + C]);
            } else if (abordagem == 2) {
                printf(" %2d ", M[C * Altura + L]);
            }
        }
        printf("\n");
    }
}

void PreencheMatriz(int Altura, int Largura, int *M, int Valor, int abordagem) {
    int L, C; //Linha e Coluna

    if (abordagem == 1) {
        for (L = 0; L < Altura; L++) {
            for (C = 0; C < Largura; C++) {
                M[L * Largura + C] = Valor;
            }
        }
    } else if (abordagem == 2) {
        for (C = 0; C < Largura; C++) {
            for (L = 0; L < Altura; L++) {
                M[C * Altura + L] = Valor;
            }
        }
    }

}

int main(int argc, char **argv) {

    int abrMatrizA = 0, abrMatrizB = 0;

    abrMatrizA = atoi(argv[1]);
    abrMatrizB = atoi(argv[2]);
    char *nomeArquivo = argv[3];
    char *texto = argv[4];

    for (int quantidade = 1000; quantidade <= 7000; quantidade = quantidade + 1000) {
        for (int i = 0; i < 3; i++) {

            int *Ma = (int *) malloc(sizeof(int) * quantidade * quantidade);
            if (Ma == NULL) {
                printf("\nErro alocando memoria! \n");
                exit(EXIT_FAILURE);
            }

            int *Mb = (int *) malloc(sizeof(int) * quantidade * quantidade);
            if (Mb == NULL) {
                printf("\nErro alocando memoria! \n");
                exit(EXIT_FAILURE);
            }

            int *Mc = (int *) malloc(sizeof(int) * quantidade * quantidade);
            if (Mc == NULL) {
                printf("\nErro alocando memoria! \n");
                exit(EXIT_FAILURE);
            }

            printf("::: Matrix Multiplication \n");
            printf("::: Width: %d | Height: %d | Total Size: %d \n", quantidade, quantidade, quantidade * quantidade);
            printf("::: Preenchendo as matrizes... ");


            PreencheMatriz(quantidade, quantidade, Ma, 1, abrMatrizA);
            PreencheMatriz(quantidade, quantidade, Mb, 3, abrMatrizB);

            //PreencheMatriz(N, N, Mc, 0);

            printf("Done! Now calculating...\n");
            //MostraMatriz(N, N, Ma, " Matriz A ");
            //MostraMatriz(N, N, Mb, " Matriz B ");
            //MostraMatriz(N, N, Mc, " Matriz C ");

            int L, C, K;

            // conta o tempo de processamento
            clock_t tempo;
            tempo = clock();

            //C = A . B
            for (L = 0; L < quantidade; L++) {
                for (C = 0; C < quantidade; C++) {
                    for (K = 0; K < quantidade; K++) {
                        Mc[L * quantidade + C] += Ma[L * quantidade + K] * Mb[K * quantidade + C];
                    }
                }
            }

            clock_t final = clock();
            double tempoProcessamento = ((double) (final - tempo)) / (double) (CLOCKS_PER_SEC);

            SalvaMatriz(nomeArquivo, texto, quantidade, tempoProcessamento);
            printf("\nAll done! Tempo de processamento: %f segundos.\n", tempoProcessamento);
        }
    }
    return 0;
}
